package vendingMachine;

public enum Coin {
    PENNY(false, 1),
    NICKLE(true, 5),
    DIME(true, 10),
    QUARTER(true, 25);

    private final boolean acceptable;
    private final int value;

    Coin(boolean acceptable, int value) {
        this.acceptable = acceptable;
        this.value = value;
    }

    public boolean isAcceptable() {
        return acceptable;
    }

    public int getValue() {
        return value;
    }
}
