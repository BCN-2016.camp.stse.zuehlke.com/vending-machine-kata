package vendingMachine;

import java.util.Locale;
import java.util.Objects;

/**
 * Created by stse on 12.05.16.
 */
public class VendingMachine {
    private boolean lastCoinAccepted = false;
    private long total = 0;
    private CashSlot cashSlot;

    public VendingMachine(CashSlot cashSlot) {
        this.cashSlot = Objects.requireNonNull(cashSlot);
    }

    public void insert(Coin coin) {
        lastCoinAccepted = false;
        lastCoinAccepted = coin.isAcceptable();
        if (lastCoinAccepted) {
            total += coin.getValue();
        } else {
            cashSlot.add(coin);
        }
    }

    public boolean isLastCoinAccepted() {
        return lastCoinAccepted;
    }

    public long getTotal() {
        return total;
    }

    public String getDisplayText() {
        return String.format(Locale.ENGLISH, "You inserted $%.2f", total / 100.00f);
    }

    public CashSlot getCashSlot() {
        return cashSlot;
    }
}
