package vendingMachine;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Created by stse on 12.05.16.
 */
public class CashSlot {
    private Collection<Coin> contents = new LinkedList<>();

    public Collection<Coin> getContents() {
        return Collections.unmodifiableCollection(contents);
    }

    public void add(Coin coin) {
        contents.add(coin);
    }
}
