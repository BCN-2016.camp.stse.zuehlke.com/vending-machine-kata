package vendingMachine;

import org.junit.Test;

import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;

/**
 * Created by stse on 12.05.16.
 */
public class CashSlotTest {
    @Test
    public void checkAddedCoinsAreInTheSlot() throws Exception {
        CashSlot cashSlot = new CashSlot();

        cashSlot.add(Coin.PENNY);

        assertThat(cashSlot.getContents(), hasItem(Coin.PENNY));
    }

}