package vendingMachine;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Created by stse on 12.05.16.
 */
public class VendingMachineTest {
    @Test
    public void insert() throws Exception {
        VendingMachine vendingMachine = new VendingMachine(new CashSlot());

        vendingMachine.insert(Coin.NICKLE);
        assertEquals(Coin.NICKLE.getValue(), vendingMachine.getTotal());
        assertTrue(vendingMachine.isLastCoinAccepted());
        assertThat(vendingMachine.getCashSlot().getContents(), is((empty())));

        vendingMachine.insert(Coin.DIME);
        assertEquals(Coin.NICKLE.getValue() + Coin.DIME.getValue(), vendingMachine.getTotal());
        assertTrue(vendingMachine.isLastCoinAccepted());
        assertThat(vendingMachine.getCashSlot().getContents(), is((empty())));

        vendingMachine.insert(Coin.QUARTER);
        assertEquals(Coin.NICKLE.getValue() + Coin.DIME.getValue() + Coin.QUARTER.getValue() , vendingMachine.getTotal());
        assertTrue(vendingMachine.isLastCoinAccepted());
        assertThat(vendingMachine.getCashSlot().getContents(), is((empty())));

        vendingMachine.insert(Coin.PENNY);
        assertEquals(Coin.NICKLE.getValue() + Coin.DIME.getValue() + Coin.QUARTER.getValue() , vendingMachine.getTotal());
        assertFalse(vendingMachine.isLastCoinAccepted());
        assertThat(vendingMachine.getCashSlot().getContents(), hasItem(Coin.PENNY));
    }




}