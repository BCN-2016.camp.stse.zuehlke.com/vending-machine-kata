package vendingMachine;

import org.junit.Test;
import vendingMachine.Coin;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by stse on 12.05.16.
 */
public class CoinTest {

    @Test
    public void penniesAreRejected() {
        assertFalse(Coin.PENNY.isAcceptable());
    }

    @Test
    public void nicklesAreAccepted() {
        assertTrue(Coin.NICKLE.isAcceptable());
    }

    @Test
    public void dimesAreAccepted() {
        assertTrue(Coin.DIME.isAcceptable());
    }

    @Test
    public void quartersAreAccepted() {
        assertTrue(Coin.QUARTER.isAcceptable());
    }

}
