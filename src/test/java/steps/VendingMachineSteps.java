package steps;

import cucumber.api.Transform;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import vendingMachine.CashSlot;
import vendingMachine.Coin;
import vendingMachine.VendingMachine;

import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class VendingMachineSteps {

    private VendingMachine vendingMachine = new VendingMachine(new CashSlot());

    @When("^I insert a (\\w*)$")
    public void i_insert_a(@Transform(CoinTransformer.class) Coin coin) throws Throwable {
        vendingMachine.insert(coin);
    }

    @Then("^the coin should be (accepted|rejected)$")
    public void the_coin_should_be(@Transform(BooleanTransformer.class) boolean accepted) throws Throwable {
        assertEquals(accepted, vendingMachine.isLastCoinAccepted());
    }

    @Then("^the current total amount should be \\$(\\d+\\.\\d+)$")
    public void the_current_total_amount_should_be(
            @Transform(MoneyAmountTransformer.class) int total) throws Throwable {

        assertEquals(total, vendingMachine.getTotal());
    }

    @Then("^(.*) should be displayed$")
    public void you_inserted_$_should_be_displayed(String expected) throws Throwable {

        assertEquals(expected, vendingMachine.getDisplayText());
    }

    @Then("^the penny is returned$")
    public void the_penny_is_returned() throws Throwable {
        assertThat(vendingMachine.getCashSlot().getContents(), hasItem(Coin.PENNY));
    }

}

