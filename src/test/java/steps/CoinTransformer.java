package steps;

import cucumber.api.Transformer;
import vendingMachine.Coin;

/**
 * Created by stse on 12.05.16.
 */
public class CoinTransformer extends Transformer<Coin> {
    public Coin transform(String coin) {
        return Coin.valueOf(coin.toUpperCase());
    }
}
