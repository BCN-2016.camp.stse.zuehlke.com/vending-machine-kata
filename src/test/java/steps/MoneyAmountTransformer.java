package steps;

import cucumber.api.Transformer;
import vendingMachine.Coin;

/**
 * Created by stse on 12.05.16.
 */
public class MoneyAmountTransformer extends Transformer<Integer> {
    public Integer transform(String amount) {
        return Integer.valueOf(amount.replace(".", ""));
    }
}
