package steps;

import cucumber.api.Transformer;

/**
 * Created by stse on 12.05.16.
 */
public class BooleanTransformer extends Transformer<Boolean> {
    public Boolean transform(String input) {
        switch (input) {
            case "accepted":
                return true;
            case "rejected":
                return false;
            default:
                throw new IllegalArgumentException("Cannot convert " + input + " to boolean");
        }
    }
}
