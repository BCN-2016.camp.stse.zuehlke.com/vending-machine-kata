Feature: Coin
  As a vendor
  I want a vending machine that accepts coins
  So that I can collect money from the customer

  Scenario Template: Insert <coin>
    When I insert a <coin>
    Then the coin should be <action>
    And  the current total amount should be $<amount>
    And  <message> should be displayed

    Scenarios:
      | coin    | action   | amount | message            |
      | nickle  | accepted | 0.05      | You inserted $0.05 |
      | dime    | accepted | 0.10     | You inserted $0.10 |
      | quarter | accepted | 0.25     | You inserted $0.25 |

    Scenarios:
      | coin  | action   | amount | message            |
      | penny | rejected | 0.00      | You inserted $0.00 |

    Scenario: Insert multiple coins
      When I insert a penny
      Then the coin should be rejected
      And the penny is returned

      And I insert a nickle
      Then the coin should be accepted
      And I insert a dime
      Then the coin should be accepted
      And I insert a quarter
      Then the coin should be accepted
      And  the current total amount should be $0.40
      And  You inserted $0.40 should be displayed